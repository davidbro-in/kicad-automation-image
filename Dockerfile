FROM ubuntu:18.04

LABEL maintainer davidmbroin@gmail.com

RUN apt update &&\
    DEBIAN_FRONTEND=noninteractive apt install -y \
        software-properties-common \
        xserver-xorg-video-dummy\
        libgtk-3-dev \
        libcairo2-dev \
        libgit2-dev \
        transfig \
        imagemagick \
        make \
        pkg-config \
        gcc \
        python \
        zip \
        git  &&\
    add-apt-repository --yes ppa:js-reynaud/kicad-5.1 &&\
    apt update &&\
    apt install -y --install-recommends kicad &&\
    rm -rf /var/lib/apt/list/* 

COPY cachelib-undersocre.patch /cachelib-undersocre.patch
RUN git clone https://gitlab.com/neo900/eeshow
RUN cd eeshow &&\
    git apply ../cachelib-undersocre.patch &&\
    make && make install

COPY kicad_cicd /kicad_cicd
COPY libs /libs

# References
#https://hub.docker.com/r/akshmakov/kicad-docker/dockerfile
#https://hub.docker.com/r/akshmakov/pcb-cicd/dockerfile
#https://hub.docker.com/r/akshmakov/eeshow/dockerfile
#https://forum.kicad.info/t/fixing-eeshow-library-lookup-not-found-another-option/21227/5
#https://gitlab.com/dowster/eeshow/-/commit/08dea67102666e3eb839add8b29288b07fcb6f76.diff
